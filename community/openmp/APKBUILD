# Contributor: Leo <thinkabit.ukim@gmail.com>
# Maintainer: psykose <alice@ayaya.dev>
pkgname=openmp
pkgver=15.0.1
pkgrel=0
_llvmver=${pkgver%%.*}
pkgdesc="LLVM OpenMP Runtime Library"
url="https://openmp.llvm.org/"
arch="all !s390x" # LIBOMP_ARCH = UnknownArchitecture
license="Apache-2.0"
options="!check" # 6 tests failing
depends_dev="$pkgname"
makedepends="
	clang
	cmake
	elfutils-dev
	libffi-dev
	llvm$_llvmver-dev
	llvm$_llvmver-static
	perl
	samurai
	"
checkdepends="llvm$_llvmver-test-utils"
subpackages="$pkgname-dev"
source="https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver/openmp-$pkgver.src.tar.xz
	https://github.com/llvm/llvm-project/releases/download/llvmorg-$pkgver/cmake-$pkgver.src.tar.xz
	"
builddir="$srcdir/$pkgname-$pkgver.src"

build() {
	if [ "$CBUILD" != "$CHOST" ]; then
		CMAKE_CROSSOPTS="-DCMAKE_SYSTEM_NAME=Linux -DCMAKE_HOST_SYSTEM_NAME=Linux"
	fi
	CC=clang \
	CXX=clang++ \
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_MODULE_PATH="$srcdir/cmake-$pkgver.src/Modules" \
		-DLIBOMP_INSTALL_ALIASES=OFF \
		-DCLANG_TOOL=/usr/bin/clang \
		-DLINK_TOOL=/usr/bin/llvm-link \
		-DOPT_TOOL=/usr/bin/opt \
		-DOPENMP_LLVM_TOOLS_DIR=/usr/lib/llvm$_llvmver/bin \
		$CMAKE_CROSSOPTS
	cmake --build build
}

check() {
	cmake --build build --target check-openmp
}

package() {
	DESTDIR="$pkgdir" cmake --install build
	rm -f "$pkgdir"/usr/lib/libarcher_static.a
}

sha512sums="
367b540b4dbcfe197933abca885aa87c44ec76b5a54233b8e44bc9cb8ddca44a815d25ef9e92e659f6bc2667225a3012bf14f19c90fd971fc658023492775508  openmp-15.0.1.src.tar.xz
fbb29395a337be4e591567cc0f990857a2663cb2335b5ef30945c6b516dbc65e86f022ef3acc1dc572cf6791e1cd20f6754256e00b60cdbf579c04ed74460522  cmake-15.0.1.src.tar.xz
"
